Material to build TimescaleDB 1.7.2 for PostgreSQL 9.6
======================================================

TimescaleDB authors don't provide Debian packaging script to make deb package for Debian, so we have to collect from many sources:

1. Source tarball is downloaded from https://launchpad.net/%7Etimescale/+archive/ubuntu/timescaledb-ppa/+sourcepub/11416611/+listing-archive-extra. It is for Ubuntu 18.04.

2. Pre-built package for Debian is downloaded from https://packagecloud.io/timescale/timescaledb/packages/debian/stretch/timescaledb-1.7.2-postgresql-9.6_1.7.2~debian9_amd64.deb

## How to build

- Extract package 1 to get source folder and "debian" folder

```
❯ exa -TL1 timescaledb-postgresql-9.6_1.7.2
timescaledb-postgresql-9.6_1.7.2
├── appveyor.yml
├── bootstrap
├── bootstrap.bat
├── CHANGELOG.md
├── CMakeLists.txt
├── CONTRIBUTING.md
├── debian
├── docs
├── LICENSE
├── LICENSE-APACHE
├── NOTICE
├── README.md
├── scripts
├── sql
├── src
├── test
├── timescaledb.control.in
├── tsl
└── version.config
```

- Extract package 2 (*timescaledb-1.7.2-postgresql-9.6_1.7.2~debian9_amd64.deb*), to get the _changelog_ file and copy to the _debian_ folder.
- Edit _debian/control_ file, replace "amd64" with "any" or "armhf".
- Run `debuild -us -uc` to build.

There is no build instruction for `timescaledb-tools` because the package in Ubuntu PPA is provided as prebuilt binary, and that binary is for amd64 only.